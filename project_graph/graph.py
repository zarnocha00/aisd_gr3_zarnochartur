from enum import Enum
from typing import Any, Callable, Optional, Dict, List
from FIFO import Queue
import graphviz


class EdgeType(Enum):
    directed = 1
    undirected = 2


class Vertex:
    def __init__(self, data, index) -> None:
        self.data: Any = data
        self.index: int = index

    def __repr__(self) -> str:
        return_string = ""
        return_string += f'{self.data}'
        return return_string


class Edge:
    def __init__(self, source, destination, weight: Optional[float] = None) -> None:
        self.source: Vertex = source
        self.destination: Vertex = destination
        self.weight: Optional[float] = weight

    def __repr__(self) -> str:
        return_string = ""
        if self.weight is None:
            return_string += f'Z: {self.source} - DO: {self.destination}'
        else:
            return_string += f'Z: {self.source} - DO: {self.destination} - WT {self.weight}'
        return return_string


class Graph:
    def __init__(self) -> None:
        self.adjacencies: Dict[Vertex, List[Edge]] = {}

    def create_vertex(self, data: Any) -> Vertex:
        if bool(self.adjacencies) is False:
            self.adjacencies.update({Vertex(data, 0): []})
            return Vertex(data, 0)
        else:
            idx_list = list(self.adjacencies.keys())
            last_idx = idx_list[-1].index
            last_idx += 1
            self.adjacencies.update({Vertex(data, last_idx): []})
            return Vertex(data, index=last_idx)

    def add_directed_edge(self, source: Any, destination: Any, weight: Optional[float] = None) -> None:
        wierzcholki = list(self.adjacencies.keys())
        for i in range(0, len(wierzcholki)):
            if wierzcholki[i].data is source:
                source = wierzcholki[i]
            if wierzcholki[i].data is destination:
                destination = wierzcholki[i]

        edge = Edge(source, destination, weight)
        self.adjacencies.get(source).append(edge)
    
    def add_undirected_edge(self, source: Any, destination: Any, weight: Optional[float] = None) -> None:
        wierzcholki = list(self.adjacencies.keys())
        for i in range(0, len(wierzcholki)):
            if wierzcholki[i].data is source:
                source = wierzcholki[i]
            if wierzcholki[i].data is destination:
                destination = wierzcholki[i]
        edge1 = Edge(source, destination, weight)
        edge2 = Edge(destination, source, weight)
        self.adjacencies.get(source).append(edge1)
        self.adjacencies.get(destination).append(edge2)

    def traverse_breadth_first(self, visit: Callable[[Any], None]) -> None:
        kolej = Queue()
        visited_vertexes = []

        lista_wierzch = list(self.adjacencies.keys())

        visited_vertexes.append(lista_wierzch[0])
        kolej.enqueue(lista_wierzch[0])

        while len(kolej) > 0:
            v = kolej.dequeue().value
            visit(v)
            lista_kraw = self.adjacencies.get(v)
            for edge in lista_kraw:
                if not(edge.destination in visited_vertexes):
                    visited_vertexes.append(edge.destination)
                    kolej.enqueue(edge.destination)

    def dfs(self, v: Vertex, visited: List[Vertex], visit: Callable[[Any], None]):
        visit(v)
        visited.append(v)

        lista_kraw = self.adjacencies.get(v)
        for edge in lista_kraw:
            neighbour = edge.destination
            if not(neighbour in visited):
                self.dfs(neighbour, visited, visit)

    def traverse_depth_first(self, visit: Callable[[Any], None]) -> None:
        self.dfs(list(self.adjacencies.keys())[0], [], visit)

    def add(self, edge: EdgeType, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        if edge == 1 or edge == "directed":
            self.add_directed_edge(source, destination, weight)

        elif edge == 2 or edge == "undirected":
            self.add_undirected_edge(source, destination, weight)

    def show(self, name: str = 'Graph', format: str = 'png'):
        def graphviz_add_nodes(graph: Graph, vizgraph: graphviz.Digraph):
            lista_wierzch = list(graph.adjacencies.keys())
            for v in range(0, len(lista_wierzch)):
                # vizgraph.node(str(list(graph.adjacencies.keys())[i].index), f'v{(str(list(graph.adjacencies.keys())[i].index))}')
                vizgraph.node(str(list(graph.adjacencies.keys())[v].index), f'v{(str(list(graph.adjacencies.keys())[v].index))}|{(str(list(graph.adjacencies.keys())[v].data))}')

        def graphviz_add_directed_edges(graph: Graph, vizgraph: graphviz.Digraph):
            lista_wierzch = list(graph.adjacencies.keys())
            for v in lista_wierzch:
                lista_kraw = graph.adjacencies.get(v)
                for edge in lista_kraw:
                    if edge.weight is None:
                        vizgraph.edge(str(edge.source.index), str(edge.destination.index))
                    else:
                        vizgraph.edge(str(edge.source.index), str(edge.destination.index), label=str(edge.weight), weight=str(edge.weight))
                    
        g = graphviz.Digraph(name=name, format=format)

        graphviz_add_nodes(self, g)
        graphviz_add_directed_edges(self, g)

        g.node_attr.update(shape='Mrecord')
        g.edge_attr.update(arrowhead='vee')

        # print("\n\n\n", g.source)

        g.render(view=True)

    def __repr__(self) -> str:
        return_string = ""
        lista_wierzch = list(self.adjacencies.keys())
        for i in range(0, len(lista_wierzch)):
            lista_kraw = self.adjacencies.get(lista_wierzch[i])
            if i == 0:
                # print(f'{i}: v{lista_wierzch[i].index} ---->', end="")
                return_string += f'{i}: v{lista_wierzch[i].index} ---->'
            else:
                # print(f'\n{i}: v{lista_wierzch[i].index} ---->', end="")
                return_string += f'\n{i}: v{lista_wierzch[i].index} ---->'
            return_string += ' ['
            for j in range(0, len(lista_kraw)):
                # print(f' {lista_kraw[j].destination.index}', end="")
                if j == 0:
                    return_string += f'{lista_kraw[j].destination.index}: v{lista_kraw[j].destination.index}'
                else:
                    return_string += f', {lista_kraw[j].destination.index}: v{lista_kraw[j].destination.index}'
            return_string += ']'
        return return_string


def printing_indexes(x):
    print(f'v{x.index}, ', end="")


def printing_data(x):
    print(f'v{x.data}, ', end="")
