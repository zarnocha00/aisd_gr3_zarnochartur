from enum import Enum
from typing import Any, Callable, Optional, Dict, List
import graphviz


class Node:
    def __init__(self, value: Any):
        self.value: Any = value
        self.next: Node = Node


class LinkedList:
    def __init__(self):
        self.head: Node = None
        self.tail: Node = None

    def push(self, value: Any) -> None:
        # umieści nowy węzeł na początku listy
        pushed_node = Node(value)
        if self.head is None:
            self.head = pushed_node
            self.tail = self.head
        else:
            zapis = self.head
            self.head = pushed_node
            pushed_node.next = zapis

    def append(self, value: Any) -> None:
        # umieści nowy węzeł na końcu listy
        appended_node = Node(value)
        if self.head is None:
            self.head = appended_node
            self.tail = self.head
        else:
            iterator = self.head
            if len(self) >= 2:
                while iterator.next is not Node:
                    iterator = iterator.next
            if iterator.next is Node:
                iterator.next = appended_node
            self.tail = iterator.next

    def node(self, at: int) -> Node:
        # zwróci węzeł znajdujący się na wskazanej pozycji
        if at <= 0:
            return self.head
        else:
            i = 0
            iterator = self.head
            while i is not at and iterator is not self.tail:
                iterator = iterator.next
                i += 1

            return iterator

    def insert(self, value: Any, after: Node) -> None:
        # wstawi nowy węzeł tuż za węzłem wskazanym w parametrze
        copy = self.head
        copy_end = self.head
        new_node = Node(value)

        while copy is not after:
            copy = copy.next
            copy_end = copy_end.next

        if copy_end.next is not Node:
            copy_end = copy_end.next
            new_node.next = copy_end
            copy.next = new_node
        else:
            self.append(value)

    def pop(self) -> Any:
        # usunie pierwszy element z listy i go zwróci
        popping_node = self.head
        if len(self) <= 1:
            self.head = None
        else:
            after_pop = popping_node.next
            self.head = after_pop

        return popping_node

    def remove_last(self) -> Any:
        # usunie ostatni element z listy i go zwróci
        returning_value = self.tail.value
        iterator = self.head
        while iterator.next is not self.tail:
            iterator = iterator.next
        # self.tail = iterator
        iterator.next = None
        self.tail = iterator
        return returning_value

    def remove(self, after: Node) -> Any:
        # usunie z listy następnik węzła przekazanego w parametrze
        if after is self.tail:
            returning_value_at_end = self.remove_last()
            return returning_value_at_end

        iterator = self.head
        while iterator is not after:
            iterator = iterator.next
        returning_value = iterator.next.value
        if iterator.next.next is not None:
            iterator.next = iterator.next.next
        else:
            iterator.next = None
            self.tail = iterator
        return returning_value

    def __len__(self):
        # wywołana na obiekcie listy zwróci jej długość
        if self.head is None:
            return 0
        iterator = self.head
        length = 0
        # while iterator is not self.tail:
        while iterator is not Node:
            length += 1
            iterator = iterator.next
        # length += 1
        return length


class Queue:
    def __init__(self):
        self._storage = LinkedList()

    def peek(self) -> Any:
        # zwróci wartość pierwszego elementu w kolejce
        return self._storage.node(0)

    def enqueue(self, element: Any) -> None:
        # umieści nowy element na końcu kolejki
        self._storage.append(element)

    def dequeue(self) -> Any:
        # zwróci i usunie pierwszy element w kolejce
        return self._storage.pop()

    def __len__(self):
        # zwróci jej liczebność
        return len(self._storage)


class EdgeType(Enum):
    directed = 1
    undirected = 2


class Vertex:
    def __init__(self, data, index) -> None:
        self.data: Any = data
        self.index: int = index

    def __repr__(self) -> str:
        return_string = ""
        return_string += f'{self.data}'
        return return_string


class Edge:
    def __init__(self, source, destination, weight: Optional[float] = None) -> None:
        self.source: Vertex = source
        self.destination: Vertex = destination
        self.weight: Optional[float] = weight

    def __repr__(self) -> str:
        return_string = ""
        if self.weight is None:
            return_string += f'Z: {self.source} - DO: {self.destination}'
        else:
            return_string += f'Z: {self.source} - DO: {self.destination} - WT {self.weight}'
        return return_string


class Graph:
    def __init__(self) -> None:
        self.adjacencies: Dict[Vertex, List[Edge]] = {}

    def create_vertex(self, data: Any) -> Vertex:
        if bool(self.adjacencies) is False:
            self.adjacencies.update({Vertex(data, 0): []})
            return Vertex(data, 0)
        else:
            idx_list = list(self.adjacencies.keys())
            last_idx = idx_list[-1].index
            last_idx += 1
            self.adjacencies.update({Vertex(data, last_idx): []})
            return Vertex(data, index=last_idx)

    def add_directed_edge(self, source: Any, destination: Any, weight: Optional[float] = None) -> None:
        wierzcholki = list(self.adjacencies.keys())
        for i in range(0, len(wierzcholki)):
            if wierzcholki[i].data is source:
                source = wierzcholki[i]
            if wierzcholki[i].data is destination:
                destination = wierzcholki[i]

        edge = Edge(source, destination, weight)
        self.adjacencies.get(source).append(edge)

    def add_undirected_edge(self, source: Any, destination: Any, weight: Optional[float] = None) -> None:
        wierzcholki = list(self.adjacencies.keys())
        for i in range(0, len(wierzcholki)):
            if wierzcholki[i].data is source:
                source = wierzcholki[i]
            if wierzcholki[i].data is destination:
                destination = wierzcholki[i]
        edge1 = Edge(source, destination, weight)
        edge2 = Edge(destination, source, weight)
        self.adjacencies.get(source).append(edge1)
        self.adjacencies.get(destination).append(edge2)

    def traverse_breadth_first(self, visit: Callable[[Any], None]) -> None:
        kolej = Queue()
        visited_vertexes = []

        lista_wierzch = list(self.adjacencies.keys())

        visited_vertexes.append(lista_wierzch[0])
        kolej.enqueue(lista_wierzch[0])

        while len(kolej) > 0:
            v = kolej.dequeue().value
            visit(v)
            lista_kraw = self.adjacencies.get(v)
            for edge in lista_kraw:
                if not (edge.destination in visited_vertexes):
                    visited_vertexes.append(edge.destination)
                    kolej.enqueue(edge.destination)

    def dfs(self, v: Vertex, visited: List[Vertex], visit: Callable[[Any], None]):
        visit(v)
        visited.append(v)

        lista_kraw = self.adjacencies.get(v)
        for edge in lista_kraw:
            neighbour = edge.destination
            if not (neighbour in visited):
                self.dfs(neighbour, visited, visit)

    def traverse_depth_first(self, visit: Callable[[Any], None]) -> None:
        self.dfs(list(self.adjacencies.keys())[0], [], visit)

    def add(self, edge: EdgeType, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        if edge == 1 or edge == "directed":
            self.add_directed_edge(source, destination, weight)

        elif edge == 2 or edge == "undirected":
            self.add_undirected_edge(source, destination, weight)

    def show(self, name: str = 'Graph', format: str = 'png'):
        def graphviz_add_nodes(graph: Graph, vizgraph: graphviz.Digraph):
            lista_wierzch = list(graph.adjacencies.keys())
            for v in range(0, len(lista_wierzch)):
                # vizgraph.node(str(list(graph.adjacencies.keys())[i].index), f'v{(str(list(graph.adjacencies.keys())[i].index))}')
                vizgraph.node(str(list(graph.adjacencies.keys())[v].index), f'v{(str(list(graph.adjacencies.keys())[v].index))}|{(str(list(graph.adjacencies.keys())[v].data))}')

        def graphviz_add_directed_edges(graph: Graph, vizgraph: graphviz.Digraph):
            lista_wierzch = list(graph.adjacencies.keys())
            for v in lista_wierzch:
                lista_kraw = graph.adjacencies.get(v)
                for edge in lista_kraw:
                    if edge.weight is None:
                        vizgraph.edge(str(edge.source.index), str(edge.destination.index))
                    else:
                        vizgraph.edge(str(edge.source.index), str(edge.destination.index), label=str(edge.weight),
                                      weight=str(edge.weight))

        g = graphviz.Digraph(name=name, format=format)

        graphviz_add_nodes(self, g)
        graphviz_add_directed_edges(self, g)

        g.node_attr.update(shape='Mrecord')
        g.edge_attr.update(arrowhead='vee')

        # print("\n\n\n", g.source)

        g.render(view=True)

    def __repr__(self) -> str:
        return_string = ""
        lista_wierzch = list(self.adjacencies.keys())
        for i in range(0, len(lista_wierzch)):
            lista_kraw = self.adjacencies.get(lista_wierzch[i])
            if i == 0:
                # print(f'{i}: v{lista_wierzch[i].index} ---->', end="")
                return_string += f'{i}: v{lista_wierzch[i].index} ---->'
            else:
                # print(f'\n{i}: v{lista_wierzch[i].index} ---->', end="")
                return_string += f'\n{i}: v{lista_wierzch[i].index} ---->'
            return_string += ' ['
            for j in range(0, len(lista_kraw)):
                # print(f' {lista_kraw[j].destination.index}', end="")
                if j == 0:
                    return_string += f'{lista_kraw[j].destination.index}: v{lista_kraw[j].destination.index}'
                else:
                    return_string += f', {lista_kraw[j].destination.index}: v{lista_kraw[j].destination.index}'
            return_string += ']'
        return return_string


def dead_path(g: Graph, cross_id: int) -> Optional[List[Vertex]]:
    start_vertex = list(g.adjacencies.keys())[cross_id]
    kolej = Queue()

    visited: List = []
    kolej.enqueue([start_vertex])

    while len(kolej) > 0:
        p: List = kolej.dequeue().value
        v: Vertex = p[-1]
        sasiedzi_v = list(g.adjacencies.get(v))
        for i in range(0, len(sasiedzi_v)):
            n: Vertex = sasiedzi_v[i].destination
            if n in visited:
                break
            np: List = p.copy()
            np.append(n)
            visited.append(n)
            kolej.enqueue(np)
            if n is start_vertex:
                return np

    return None


graf = Graph()
graf.create_vertex("A")
graf.create_vertex("B")
graf.create_vertex("C")
graf.create_vertex("D")
graf.create_vertex("E")
graf.create_vertex("F")
graf.create_vertex("G")

graf.add_directed_edge(list(graf.adjacencies.keys())[0], list(graf.adjacencies.keys())[1])
graf.add_directed_edge(list(graf.adjacencies.keys())[1], list(graf.adjacencies.keys())[6])
graf.add_directed_edge(list(graf.adjacencies.keys())[6], list(graf.adjacencies.keys())[2])
graf.add_directed_edge(list(graf.adjacencies.keys())[2], list(graf.adjacencies.keys())[5])
graf.add_directed_edge(list(graf.adjacencies.keys())[5], list(graf.adjacencies.keys())[3])
graf.add_directed_edge(list(graf.adjacencies.keys())[3], list(graf.adjacencies.keys())[4])
graf.add_directed_edge(list(graf.adjacencies.keys())[3], list(graf.adjacencies.keys())[0])

# graf.show('graf', 'png')
print(f'graf dead_path: {dead_path(graf, 0)}')

podroz = Graph()
podroz.create_vertex("Olsztyn")
podroz.create_vertex("Mława")
podroz.create_vertex("Warszawa")
podroz.create_vertex("Łódź")
podroz.create_vertex("Poznań")
podroz.create_vertex("Bydgoszcz")
podroz.create_vertex("Gdańsk")
podroz.create_vertex("Elbląg")
podroz.create_vertex("Częstochowa")

podroz.add_directed_edge("Olsztyn", "Mława")
podroz.add_directed_edge("Mława", "Warszawa")
podroz.add_directed_edge("Mława", "Elbląg")
podroz.add_directed_edge("Warszawa", "Łódź")
podroz.add_directed_edge("Warszawa", "Poznań")
podroz.add_directed_edge("Łódź", "Bydgoszcz")
podroz.add_directed_edge("Łódź", "Częstochowa")
podroz.add_directed_edge("Częstochowa", "Warszawa")
podroz.add_directed_edge("Poznań", "Bydgoszcz")
podroz.add_directed_edge("Bydgoszcz", "Gdańsk")
podroz.add_directed_edge("Bydgoszcz", "Warszawa")
podroz.add_directed_edge("Bydgoszcz", "Olsztyn")
podroz.add_directed_edge("Gdańsk", "Olsztyn")
podroz.add_directed_edge("Gdańsk", "Elbląg")
podroz.add_directed_edge("Elbląg", "Bydgoszcz")

# podroz.show('podroz', 'png')
print(f'podroz dead_path: {dead_path(podroz, 1)}')  # do Mławy

graf2 = Graph()

graf2.create_vertex(1)
graf2.create_vertex(2)
graf2.create_vertex(3)
graf2.create_vertex(4)

graf2.add("directed", 1, 2)
graf2.add("directed", 2, 3)
graf2.add("directed", 3, 4)
graf2.add("directed", 4, 1)

# graf2.show('graf2', 'png')
print(f'graf2 dead_path: {dead_path(graf2, 0)}')
