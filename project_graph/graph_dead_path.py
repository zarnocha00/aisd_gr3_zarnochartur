from typing import Optional, List
from graph import Graph, Vertex
from FIFO import Queue


def dead_path(g: Graph, cross_id: int) -> Optional[List[Vertex]]:
    start_vertex = list(g.adjacencies.keys())[cross_id]
    kolej = Queue()

    visited: List = []
    kolej.enqueue([start_vertex])

    while len(kolej) > 0:
        p: List = kolej.dequeue().value
        v: Vertex = p[-1]
        sasiedzi_v = list(g.adjacencies.get(v))
        for i in range(0, len(sasiedzi_v)):
            n = sasiedzi_v[i].destination
            if n in visited:
                break
            np: List = p.copy()
            np.append(n)
            visited.append(n)
            kolej.enqueue(np)
            if n is start_vertex:
                return np

    return None


graf = Graph()
graf.create_vertex("A")
graf.create_vertex("B")
graf.create_vertex("C")
graf.create_vertex("D")
graf.create_vertex("E")
graf.create_vertex("F")
graf.create_vertex("G")

graf.add_directed_edge(list(graf.adjacencies.keys())[0], list(graf.adjacencies.keys())[1])
graf.add_directed_edge(list(graf.adjacencies.keys())[1], list(graf.adjacencies.keys())[6])
graf.add_directed_edge(list(graf.adjacencies.keys())[6], list(graf.adjacencies.keys())[2])
graf.add_directed_edge(list(graf.adjacencies.keys())[2], list(graf.adjacencies.keys())[5])
graf.add_directed_edge(list(graf.adjacencies.keys())[5], list(graf.adjacencies.keys())[3])
graf.add_directed_edge(list(graf.adjacencies.keys())[3], list(graf.adjacencies.keys())[4])
graf.add_directed_edge(list(graf.adjacencies.keys())[3], list(graf.adjacencies.keys())[0])

# graf.show()
print(dead_path(graf, 0))

podroz = Graph()
podroz.create_vertex("Olsztyn")
podroz.create_vertex("Mława")
podroz.create_vertex("Warszawa")
podroz.create_vertex("Łódź")
podroz.create_vertex("Poznań")
podroz.create_vertex("Bydgoszcz")
podroz.create_vertex("Gdańsk")
podroz.create_vertex("Elbląg")
podroz.create_vertex("Częstochowa")

podroz.add_directed_edge("Olsztyn", "Mława")
podroz.add_directed_edge("Mława", "Warszawa")
podroz.add_directed_edge("Mława", "Elbląg")
podroz.add_directed_edge("Warszawa", "Łódź")
podroz.add_directed_edge("Warszawa", "Poznań")
podroz.add_directed_edge("Łódź", "Bydgoszcz")
podroz.add_directed_edge("Łódź", "Częstochowa")
podroz.add_directed_edge("Częstochowa", "Warszawa")
podroz.add_directed_edge("Poznań", "Bydgoszcz")
podroz.add_directed_edge("Bydgoszcz", "Gdańsk")
podroz.add_directed_edge("Bydgoszcz", "Warszawa")
podroz.add_directed_edge("Bydgoszcz", "Olsztyn")
podroz.add_directed_edge("Gdańsk", "Olsztyn")
podroz.add_directed_edge("Gdańsk", "Elbląg")
podroz.add_directed_edge("Elbląg", "Bydgoszcz")

# podroz.show()
print(dead_path(podroz, 1))  # do Mławy

graf2 = Graph()

graf2.create_vertex(1)
graf2.create_vertex(2)
graf2.create_vertex(3)
graf2.create_vertex(4)

graf2.add("directed", 1, 2)
graf2.add("directed", 2, 3)
graf2.add("directed", 3, 4)
graf2.add("directed", 4, 1)

graf2.show()
print(dead_path(graf2, 0))
