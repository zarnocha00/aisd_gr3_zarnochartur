from typing import Any, Callable, List

from binarytree import build2
# import binarytree
from FIFO import Queue


class BinaryNode:
    value: Any
    left_child: 'BinaryNode'
    right_child: 'BinaryNode'

    def __init__(self, value):
        self.value = value
        self.left_child = None
        self.right_child = None

    def is_leaf(self) -> bool:
        if self.left_child is None or self.right_child is None:
            return True
        return False

    def add_left_child(self, value: Any) -> None:
        self.left_child = BinaryNode(value)

    def add_right_child(self, value: Any) -> None:
        self.right_child = BinaryNode(value)

    def traverse_in_order(self, visit: Callable[[Any], None]):
        # wykona operację poprzecznego przejścia po podrzędnych węzłach

        # jeżeli bieżący węzeł ma lewe dziecko, to wykonaj na nim metodę in_order
        # odwiedź bieżący węzeł
        # jeżeli bieżący węzeł ma prawe dziecko, to wykonaj na nim metodę in_order

        if self.left_child is not None:
            self.left_child.traverse_in_order(visit)
        visit(self)
        if self.right_child is not None:
            self.right_child.traverse_in_order(visit)

    def traverse_post_order(self, visit: Callable[[Any], None]):
        # wykona operację wstecznego przejścia po podrzędnych węzłach

        # jeżeli bieżący węzeł ma lewe dziecko, to wykonaj na nim metodę post_order
        # jeżeli bieżący węzeł ma prawe dziecko, to wykonaj na nim metodę post_order
        # odwiedź bieżący węzeł

        if self.left_child is not None:
            self.left_child.traverse_post_order(visit)

        if self.right_child is not None:
            self.right_child.traverse_post_order(visit)

        visit(self)

    def traverse_pre_order(self, visit: Callable[[Any], None]):
        # wykona operację wzdłużnego przejścia po podrzędnych węzłach

        # odwiedź bieżący węzeł
        # jeżeli bieżący węzeł ma lewe dziecko, to wykonaj na nim metodę pre_order
        # jeżeli bieżący węzeł ma prawe dziecko, to wykonaj na nim metodę pre_order

        visit(self)
        if self.left_child is not None:
            self.left_child.traverse_pre_order(visit)
        if self.right_child is not None:
            self.right_child.traverse_pre_order(visit)

    def travese_from_left(self, lista: List) -> List:
        if self.left_child is not None:
            self.left_child.travese_from_left(lista)
        lista.append(self.value)

        return lista

    def travese_to_right(self, lista: List):
        lista.append(self.value)
        if self.right_child is not None:
            self.right_child.travese_to_right(lista)

        return lista

    def get_parent(self, node: 'BinaryNode') -> 'BinaryNode':
        kolejka = Queue()
        kolejka.enqueue(self)

        while len(kolejka) > 0:
            iterator = kolejka.dequeue().value

            if iterator is not None:
                if iterator.left_child is not None:
                    if iterator.left_child.value != node.value:
                        kolejka.enqueue(iterator.left_child)
                    elif iterator.left_child == node:
                        return iterator 

                if iterator.right_child is not None:
                    if iterator.right_child.value is not node.value:
                        kolejka.enqueue(iterator.right_child)
                    elif iterator.right_child == node:
                        return iterator

        return BinaryNode(None)


class BinaryTree:
    root: BinaryNode

    def __init__(self, value):
        self.root = BinaryNode(value)

    def traverse_in_order(self, visit: Callable[[Any], None]):
        self.root.traverse_in_order(visit)

    def traverse_post_order(self, visit: Callable[[Any], None]):
        self.root.traverse_post_order(visit)

    def traverse_pre_order(self, visit: Callable[[Any], None]):
        self.root.traverse_pre_order(visit)

    def show(self) -> None:
        kolejka = Queue()
        node_list = []
        kolejka.enqueue(self.root)

        while len(kolejka) > 0:
            node_list.append(kolejka.peek().value)
            node = kolejka.dequeue().value
            # print(f'nv: {node.value}')
            
            if node is not None:
                if node.left_child is not None:
                    kolejka.enqueue(node.left_child)
                    # if node.left_child.left_child is None and node.right_child is not None:
                        # if node.right_child is not None:
                            # print('n.l.l + i_p')

                            # kolejka.enqueue(BinaryNode(None))
                    # if node.left_child.right_child is None and node.right_child is not None:
                    #     # if node.right_child is not None:
                    #         print('n.l.r + i_p')
                    #         kolejka.enqueue(BinaryNode(None))
                        # kolejka.enqueue(node.left_child.left_child)
                        # print(f'vall: {node.left_child.left_child.value}')
                # else:
                    # if node.is_leaf() is False:
                        # kolejka.enqueue(BinaryNode(None))

                if node.right_child is not None:
                    kolejka.enqueue(node.right_child)
                else:
                    if node.is_leaf() is False:
                        kolejka.enqueue(BinaryNode(None))
            
                if self.root.get_parent(node).left_child == node:
                    kolejka.enqueue(BinaryNode(None))
                    if self.root.get_parent(self.root.get_parent(node)).left_child is not None and self.root.get_parent(self.root.get_parent(node)).left_child.left_child == node:
                        kolejka.enqueue(BinaryNode(None))

            else:
                kolejka.enqueue(BinaryNode(None))


        nowa_lista = []
        for node in node_list:
            nowa_lista.append(node.value)
        print(f'Lista wezlow drzewa: {nowa_lista}')
        binary_tree = build2(nowa_lista)
        print(binary_tree)        

    # diff. way
    # def show2(self):
    #     kolej = Queue()
    #     node_list = []
    #     kolej.enqueue(self.root)
    #
    #     while len(kolej) >= 0:
    #         # print(queue[0].value)
    #         node_list.append(kolej.peek())
    #         node = kolej.dequeue()
    #         # print(f'node: {node.value.value}')
    #         if node.value.left_child is not None:
    #             kolej.enqueue(node.value.left_child)
    #         if node.value.right_child is not None:
    #             kolej.enqueue(node.value.right_child)
    #
    #     node_value_list = []
    #     for node in node_list:
    #         node_value_list.append(node.value.value)
    #
    #     binary_tree = build(node_value_list)
    #     print(binary_tree)


def print_node(node: BinaryNode):
    print(node.value)


def top_line(tree: BinaryTree) -> List[BinaryNode]:
    lista: List[BinaryNode] = []

    tree.root.travese_from_left(lista)
    if tree.root.right_child is not None:
        tree.root.right_child.travese_to_right(lista)

    return lista


# tree z lab5

tree = BinaryTree(10)
assert tree.root.value == 10
tree.root.add_right_child(2)

tree.root.right_child.add_left_child(4)
tree.root.right_child.add_right_child(6)

tree.root.add_left_child(9)
tree.root.left_child.add_left_child(1)
tree.root.left_child.add_right_child(3)

assert tree.root.right_child.is_leaf() is False
assert tree.root.left_child.left_child.value == 1
assert tree.root.left_child.left_child.is_leaf() is True

print("top_line(tree): ")
print(top_line(tree))

print("\ntree.root.travese_in_order():")
tree.root.traverse_in_order(print_node)

print("\ntree.root.travese_post_order():")
tree.root.traverse_post_order(print_node)

print("\ntree.root.travese_pre_order():")
tree.root.traverse_pre_order(print_node)

print("\ntree.show():")
tree.show()

# tree2
tree2 = BinaryTree(1)
tree2.root.add_right_child(3)
tree2.root.right_child.add_right_child(7)

tree2.root.add_left_child(2)
tree2.root.left_child.add_left_child(4)
tree2.root.left_child.add_right_child(5)

tree2.root.left_child.left_child.add_left_child(8)
tree2.root.left_child.left_child.add_right_child(9)

print("\ntree2.traverse_in_order: ")
tree2.traverse_in_order(print_node)

print("\ntree2.traverse_post_order")
tree2.traverse_post_order(print_node)

print("\ntree2.traverse_pre_order")
tree2.traverse_pre_order(print_node)

print("\ntop_line(tree2): ")
print(top_line(tree2))

# print("\ntree2.show(): ")
# tree2.show()

# tree3

tree3 = BinaryTree(100)

tree3.root.add_left_child(10)
tree3.root.left_child.add_right_child(8)

tree3.root.add_right_child(45)
tree3.root.right_child.add_left_child(53)

tree3.root.right_child.add_right_child(70)

tree3.root.right_child.right_child.add_left_child(81)
tree3.root.right_child.right_child.add_right_child(24)

tree3.root.left_child.add_left_child(20)

tree3.root.left_child.left_child.add_left_child(25)
tree3.root.left_child.left_child.add_right_child(15)

tree3.root.left_child.left_child.left_child.add_left_child(40)
tree3.root.left_child.left_child.left_child.add_right_child(31)

tree3.root.left_child.left_child.right_child.add_left_child(17)
tree3.root.left_child.left_child.right_child.add_right_child(21)

print("\ntop_line(tree3): ")
print(top_line(tree3))
