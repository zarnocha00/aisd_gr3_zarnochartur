from typing import Any, Callable, List

from binarytree import build, build2

from FIFO import Queue


class BinaryNode:
    def __init__(self, value):
        self.value = value
        self.left_child = None
        self.right_child = None


    def min(self) -> 'BinaryNode':
        kolejka = Queue()
        kolejka.enqueue(self)

        najmniejszy_node = self

        while len(kolejka) > 0:
            node = kolejka.dequeue().value
            if najmniejszy_node.value > node.value:
                najmniejszy_node = node
            if node is not None:
                if node.left_child is not None:
                    kolejka.enqueue(node.left_child)
    
                if node.right_child is not None:
                    kolejka.enqueue(node.right_child)

        return najmniejszy_node

    def is_parent(self) -> bool:
        if self.left_child is not None and self.right_child is not None:
            return True
        else: return False

    def is_leaf(self) -> bool:
        if self.right_child is None and self.left_child is None:
            return True
        return False

    def get_parent(self, node: 'BinaryNode') -> 'BinaryNode':
        kolejka = Queue()
        kolejka.enqueue(self)

        while len(kolejka) > 0:
            iterator = kolejka.dequeue().value

            if iterator is not None:
                if iterator.left_child is not None:
                    if iterator.left_child.value != node.value:
                        kolejka.enqueue(iterator.left_child)
                    elif iterator.left_child == node:
                        return iterator 

                if iterator.right_child is not None:
                    if iterator.right_child.value is not node.value:
                        kolejka.enqueue(iterator.right_child)
                    elif iterator.right_child == node:
                        return iterator

        return BinaryNode(None)

class BinarySearchTree:
    def __init__(self, value):
        self.root = BinaryNode(value)

    def insert(self, value: Any) -> None:
        self.root = self._insert(self.root, value)

    def _insert(self, node: BinaryNode, value: Any) -> BinaryNode:
        # nie mozna nazwac argumentu 'from', bo wystepuja bledy
        if value < node.value:
            if node.left_child is not None:
                node.left_child = self._insert(node.left_child, value)
            else:
                node.left_child = BinaryNode(value)

        else:
            if node.right_child is not None:
                node.right_child = self._insert(node.right_child, value)
            else:
                node.right_child = BinaryNode(value)
        return node

    def insertlist(self, list: List[Any]) -> None:
        list.reverse()
        while len(list) > 0:
            self.insert(list.pop())

    def contains(self, val: Any) -> bool:
        if self.root.value == val:
            return True
        
        kolejka = Queue()
        kolejka.enqueue(self.root)

        while len(kolejka) > 0:
            node = kolejka.dequeue().value
            if val == node.value:
                return True
            if node is not None:
                if node.left_child is not None:
                    kolejka.enqueue(node.left_child)
    
                if node.right_child is not None:
                    kolejka.enqueue(node.right_child)

        return False

    def remove(self, value: Any) -> None:
        self.root = self._remove(self.root, value)

    def _remove(self, node: BinaryNode, val: Any) -> BinaryNode:
        if val == node.value:
            if node.is_leaf is True:
                return None
            elif node.left_child is None:
                return node.right_child
            elif node.right_child is None:
                return node.left_child
            node.value = node.right_child.min().value
            node.right_child = self._remove(node.right_child.right_child, val)

        elif val < node.value:
            if node.left_child is not None:
                node.left_child = self._remove(node.left_child, val)
            # else: return BinaryNode(None)

        elif val > node.value:
            if node.right_child is not None:
                node.right_child = self._remove(node.right_child, val)
            # else: return BinaryNode(None)

        return node
        
    def show(self) -> None:
        kolejka = Queue()
        node_list = []
        kolejka.enqueue(self.root)

        while len(kolejka) > 0:
            node_list.append(kolejka.peek().value)
            node = kolejka.dequeue().value
            # print(f'nv: {node.value}')
            
            if node is not None:
                if node.left_child is not None:
                    kolejka.enqueue(node.left_child)
                    # if node.left_child.left_child is None and node.right_child is not None:
                        # if node.right_child is not None:
                            # print('n.l.l + i_p')

                            # kolejka.enqueue(BinaryNode(None))
                    # if node.left_child.right_child is None and node.right_child is not None:
                    #     # if node.right_child is not None:
                    #         print('n.l.r + i_p')
                    #         kolejka.enqueue(BinaryNode(None))
                        # kolejka.enqueue(node.left_child.left_child)
                        # print(f'vall: {node.left_child.left_child.value}')
                # else:
                    # if node.is_leaf() is False:
                        # kolejka.enqueue(BinaryNode(None))

                if node.right_child is not None:
                    kolejka.enqueue(node.right_child)
                else:
                    if node.is_leaf() is False:
                        kolejka.enqueue(BinaryNode(None))
            
                if self.root.get_parent(node).left_child == node:
                    kolejka.enqueue(BinaryNode(None))
                    if self.root.get_parent(self.root.get_parent(node)).left_child is not None and self.root.get_parent(self.root.get_parent(node)).left_child.left_child == node:
                        kolejka.enqueue(BinaryNode(None))

            else:
                kolejka.enqueue(BinaryNode(None))


        nowa_lista = []
        for node in node_list:
            nowa_lista.append(node.value)
        print(f'Lista wezlow drzewa: {nowa_lista}')
        binary_tree = build2(nowa_lista)
        print(binary_tree)


tree = BinarySearchTree(8)
# tree.insert(3)
# tree.insert(10)

# tree.insert(1)
# tree.insert(6)
# tree.insert(14)

# tree.insert(4)
# tree.insert(7)
# tree.insert(13)

l = [3, 10, 1, 6, 14, 4, 7, 13]
tree.insertlist(l)

# print(tree.root.value)
# print(tree.root.left_child.value)
# print(tree.root.right_child.value)

# print(tree.root.left_child.left_child.value)
# print(tree.root.left_child.right_child.value)

# print(tree.root.right_child.left_child.value)
# print(tree.root.right_child.right_child.value)

tree.show()

# print(f'min: {tree.root.left_child.right_child.min().value}')

print(f'cont. 8?: {tree.contains(8)}')

tree.remove(8)
print('remove 8')
print(f'cont. 8?: {tree.contains(8)}')
tree.show()

# tree2 = build2([8, 3, 10, 1, 6, None, 14, None, None, 4, 7, 13])
# print(tree2)
# print(tree2.values)
# print(f'tv: {tree2.values}')
# print(f'tv2: {tree2.values2}')