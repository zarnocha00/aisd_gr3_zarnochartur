from enum import Enum
from typing import Any, Callable, Optional, Dict, List
from FIFO import Queue
import graphviz


class EdgeType(Enum):
    directed = 1
    undirected = 2


class Vertex:
    def __init__(self, data, index) -> None:
        self.data: Any = data
        self.index: int = index


class Edge:
    def __init__(self, source, destination, weight: Optional[float] = None) -> None:
        self.source: Vertex = source
        self.destination: Vertex = destination
        self.weight: Optional[float] = weight


class Graph:
    def __init__(self) -> None:
        self.adjacencies: Dict[Vertex, List[Edge]] = {}

    def create_vertex(self, data: Any) -> Vertex:
        if bool(self.adjacencies) is False:
            self.adjacencies.update({Vertex(data, 0): []})
            return Vertex(data, 0)
        else:
            idx_list = list(self.adjacencies.keys())
            last_idx = idx_list[-1].index
            last_idx += 1
            self.adjacencies.update({Vertex(data, last_idx): []})
            return Vertex(data, index=last_idx)

    def add_directed_edge(self, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        edge = Edge(source, destination, weight)
        self.adjacencies.get(source).append(edge)
    
    def add_undirected_edge(self, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        edge1 = Edge(source, destination, weight)
        edge2 = Edge(destination, source, weight)
        self.adjacencies.get(source).append(edge1)
        self.adjacencies.get(destination).append(edge2)

    def traverse_breadth_first(self, visit: Callable[[Any], None]) -> None:
        kolej = Queue()
        visited_vertexes = []

        lista_wierzch = list(self.adjacencies.keys())
        # prawdopodobnie w petli 'while' wierzcholek o idx 0 bedzie odwiedzony ponownie, wiec linia ponizej moze byc
        # do usuniecia.
        # visit(lista_wierzch[0])
        visited_vertexes.append(lista_wierzch[0])
        kolej.enqueue(lista_wierzch[0])

        while len(kolej) > 0:
            v = kolej.dequeue().value
            visit(v)
            lista_kraw = self.adjacencies.get(v)
            for edge in lista_kraw:
                if not(edge.destination in visited_vertexes):
                    visited_vertexes.append(edge.destination)
                    kolej.enqueue(edge.destination)

    def dfs(self, v: Vertex, visited: List[Vertex], visit: Callable[[Any], None]):
        visit(v)
        visited.append(v)

        lista_kraw = self.adjacencies.get(v)
        for edge in lista_kraw:
            neighbour = edge.destination
            if not(neighbour in visited):
                self.dfs(neighbour, visited, visit)

    def traverse_depth_first(self, visit: Callable[[Any], None]) -> None:
        self.dfs(list(self.adjacencies.keys())[0], [], visit)

    def add(self, edge: EdgeType, source: Vertex, destination: Vertex, weight: Optional[float] = None) -> None:
        if edge == 1 or edge == "directed":
            self.add_directed_edge(source, destination, weight)

        elif edge == 2 or edge == "undirected":
            self.add_undirected_edge(source, destination, weight)

    def show(self):
        def graphviz_add_nodes(graph: Graph, vizgraph: graphviz.Digraph):
            lista_wierzch = list(graph.adjacencies.keys())
            for v in range(0, len(lista_wierzch)):
                # vizgraph.node(str(list(graph.adjacencies.keys())[i].index), f'v{(str(list(graph.adjacencies.keys())[i].index))}')
                vizgraph.node(str(list(graph.adjacencies.keys())[v].index), f'v{(str(list(graph.adjacencies.keys())[v].index))}|{(str(list(graph.adjacencies.keys())[v].data))}')

        def graphviz_add_directed_edges(graph: Graph, vizgraph: graphviz.Digraph):
            lista_wierzch = list(graph.adjacencies.keys())
            for v in lista_wierzch:
                lista_kraw = graph.adjacencies.get(v)
                for edge in lista_kraw:
                    if edge.weight is None:
                        vizgraph.edge(str(edge.source.index), str(edge.destination.index))
                    else:
                        vizgraph.edge(str(edge.source.index), str(edge.destination.index), label= str(edge.weight), weight = str(edge.weight))
                    
        g = graphviz.Digraph('Graph', format='png')

        graphviz_add_nodes(self, g)
        graphviz_add_directed_edges(self, g)

        g.node_attr.update(shape='Mrecord')
        g.edge_attr.update(arrowhead='vee')

        # print("\n\n\n", g.source)

        g.render(directory='lab7', view=True)

    def __repr__(self) -> str:
        return_string = ""
        lista_wierzch = list(self.adjacencies.keys())
        for i in range(0, len(lista_wierzch)):
            lista_kraw = self.adjacencies.get(lista_wierzch[i])
            if i == 0:
                # print(f'{i}: v{lista_wierzch[i].index} ---->', end="")
                return_string += f'{i}: v{lista_wierzch[i].index} ---->'
            else:
                # print(f'\n{i}: v{lista_wierzch[i].index} ---->', end="")
                return_string += f'\n{i}: v{lista_wierzch[i].index} ---->'
            return_string += ' ['
            for j in range(0, len(lista_kraw)):
                # print(f' {lista_kraw[j].destination.index}', end="")
                if j == 0:
                    return_string += f'{lista_kraw[j].destination.index}: v{lista_kraw[j].destination.index}'
                else:
                    return_string += f', {lista_kraw[j].destination.index}: v{lista_kraw[j].destination.index}'
            return_string += ']'
        return return_string


def printing_indexes(x):
    print(f'v{x.index}, ', end="")


def printing_data(x):
    print(f'v{x.data}, ', end="")


# Graf z lab7:

przykladowy_graf = Graph()
print("przykladowy_graf = Graph():\n")

przykladowy_graf.create_vertex(0)
przykladowy_graf.create_vertex(1)
przykladowy_graf.create_vertex(2)
przykladowy_graf.create_vertex(3)
przykladowy_graf.create_vertex(4) 
przykladowy_graf.create_vertex(5)

# przykladowy_graf.add_directed_edge(list(przykladowy_graf.adjacencies.keys())[0], list(przykladowy_graf.adjacencies.keys())[1])
przykladowy_graf.add(1, list(przykladowy_graf.adjacencies.keys())[0], list(przykladowy_graf.adjacencies.keys())[1])
przykladowy_graf.add("directed", list(przykladowy_graf.adjacencies.keys())[0], list(przykladowy_graf.adjacencies.keys())[5])

przykladowy_graf.add("directed", list(przykladowy_graf.adjacencies.keys())[2], list(przykladowy_graf.adjacencies.keys())[1])
przykladowy_graf.add("directed", list(przykladowy_graf.adjacencies.keys())[2], list(przykladowy_graf.adjacencies.keys())[3])

przykladowy_graf.add("directed", list(przykladowy_graf.adjacencies.keys())[3], list(przykladowy_graf.adjacencies.keys())[4])

przykladowy_graf.add("directed", list(przykladowy_graf.adjacencies.keys())[4], list(przykladowy_graf.adjacencies.keys())[1])
przykladowy_graf.add("directed", list(przykladowy_graf.adjacencies.keys())[4], list(przykladowy_graf.adjacencies.keys())[5])

przykladowy_graf.add("directed", list(przykladowy_graf.adjacencies.keys())[5], list(przykladowy_graf.adjacencies.keys())[1])
przykladowy_graf.add("directed", list(przykladowy_graf.adjacencies.keys())[5], list(przykladowy_graf.adjacencies.keys())[2])

print("travese_breath_first:", end=" ")
przykladowy_graf.traverse_breadth_first(printing_indexes)

print("\ntraverse_depth_first:", end=" ")
przykladowy_graf.traverse_depth_first(printing_indexes)

print("\n\nnodes & edges")
print(przykladowy_graf)

przykladowy_graf.show()


# kolejny przykladowy graf:
print("\ng = Graph():\n")
g = Graph()

w1 = Vertex(5, 0)
w2 = Vertex(10, 1)
w3 = Vertex(3, 2)
w4 = Vertex(6, 3)

k1 = Edge(w1, w2)
k2 = Edge(w2, w3)
k3 = Edge(w3, w4)
k4 = Edge(w4, w1)

adj = {
    w1: [k1],
    w2: [k2],
    w3: [k3],
    w4: [k4]
}

g.adjacencies = adj

print("g.traverse_breadth_first():", end=" ")
g.traverse_breadth_first(printing_indexes)

print("\ng.traverse_depth_first():", end=" ")
g.traverse_depth_first(printing_indexes)

print("\n\nnodes & edges")
print(g)
g.show()


# kolejny przykladowy graf:
print("\ngraph = Graph():\n")
graph = Graph()

graph.create_vertex(5)
graph.create_vertex(10)
graph.add_directed_edge(list(graph.adjacencies.keys())[0], list(graph.adjacencies.keys())[1], None)

graph.create_vertex(15)
graph.create_vertex(20)
graph.add_undirected_edge(list(graph.adjacencies.keys())[2], list(graph.adjacencies.keys())[3], None)

graph.add_directed_edge(list(graph.adjacencies.keys())[0], list(graph.adjacencies.keys())[3])
graph.add_directed_edge(list(graph.adjacencies.keys())[1], list(graph.adjacencies.keys())[2])

graph.add("directed", list(graph.adjacencies.keys())[2], list(graph.adjacencies.keys())[0])

print("graph.traverse_breadth_first():", end=" ")
graph.traverse_breadth_first(printing_indexes)

print("\ngraph.traverse_depth_first():", end=" ")
graph.traverse_depth_first(printing_indexes)

print("\n\nnodes & edges")
print(graph)
graph.show()
