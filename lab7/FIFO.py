from typing import Any


class Node:
    def __init__(self, value: Any):
        self.value: Any = value
        self.next: Node = Node


class LinkedList:
    def __init__(self):
        self.head: Node = None
        self.tail: Node = None

    def push(self, value: Any) -> None:
        # umieści nowy węzeł na początku listy
        pushed_node = Node(value)
        if self.head is None:
            self.head = pushed_node
            self.tail = self.head
        else:
            zapis = self.head
            self.head = pushed_node
            pushed_node.next = zapis

    def append(self, value: Any) -> None:
        # umieści nowy węzeł na końcu listy
        appended_node = Node(value)
        if self.head is None:
            self.head = appended_node
            self.tail = self.head
        else:
            iterator = self.head
            if len(self) >= 2:
                while iterator.next is not Node:
                    iterator = iterator.next
            if iterator.next is Node: 
                iterator.next = appended_node
            self.tail = iterator.next

    def node(self, at: int) -> Node:
        # zwróci węzeł znajdujący się na wskazanej pozycji
        if at <= 0:
            return self.head
        else:
            i = 0
            iterator = self.head
            while i is not at and iterator is not self.tail:
                iterator = iterator.next
                i += 1

            return iterator

    def insert(self, value: Any, after: Node) -> None:
        # wstawi nowy węzeł tuż za węzłem wskazanym w parametrze
        copy = self.head
        copy_end = self.head
        new_node = Node(value)

        while copy is not after:
            copy = copy.next
            copy_end = copy_end.next

        if copy_end.next is not Node:
            copy_end = copy_end.next
            new_node.next = copy_end
            copy.next = new_node
        else:
            self.append(value)

    def pop(self) -> Any:
        # usunie pierwszy element z listy i go zwróci
        popping_node = self.head
        if len(self) <= 1:
            self.head = None
        else:
            after_pop = popping_node.next
            self.head = after_pop

        return popping_node

    def remove_last(self) -> Any:
        # usunie ostatni element z listy i go zwróci
        returning_value = self.tail.value
        iterator = self.head
        while iterator.next is not self.tail:
            iterator = iterator.next
        # self.tail = iterator
        iterator.next = None
        self.tail = iterator
        return returning_value

    def remove(self, after: Node) -> Any:
        # usunie z listy następnik węzła przekazanego w parametrze
        if after is self.tail:
            returning_value_at_end = self.remove_last()
            return returning_value_at_end

        iterator = self.head
        while iterator is not after:
            iterator = iterator.next
        returning_value = iterator.next.value
        if iterator.next.next is not None:
            iterator.next = iterator.next.next
        else:
            iterator.next = None
            self.tail = iterator
        return returning_value

    def __len__(self):
        # wywołana na obiekcie listy zwróci jej długość
        if self.head is None:
            return 0
        iterator = self.head
        length = 0
        # while iterator is not self.tail:
        while iterator is not Node:
            length += 1
            iterator = iterator.next
        # length += 1
        return length


class Queue:
    def __init__(self):
        self._storage = LinkedList()

    def peek(self) -> Any:
        # zwróci wartość pierwszego elementu w kolejce
        return self._storage.node(0)

    def enqueue(self, element: Any) -> None:
        # umieści nowy element na końcu kolejki
        self._storage.append(element)

    def dequeue(self) -> Any:
        # zwróci i usunie pierwszy element w kolejce
        return self._storage.pop()

    def __len__(self):
        # zwróci jej liczebność
        return len(self._storage)
