from typing import Any

'''
klasa Node będzie reprezentacją węzła                                                                           DONE
klasa LinkedList będzie reprezentacją listy jednokierunkowej                                                    DONE
metoda push(self, value: Any) -> None umieści nowy węzeł na początku listy                                      DONE
metoda append(self, value: Any) -> None umieści nowy węzeł na końcu listy                                       DONE
metoda node(self, at: int) -> Node zwróci węzeł znajdujący się na wskazanej pozycji                             DONE
metoda insert(self, value: Any, after: Node) -> None wstawi nowy węzeł tuż za węzłem wskazanym w parametrze     DONE
metoda pop(self) -> Any usunie pierwszy element z listy i go zwróci                                             DONE
metoda remove_last(self) -> Any usunie ostatni element z listy i go zwróci                                      DONE
metoda remove(self, after: Node) -> Any usunie z listy następnik węzła przekazanego w parametrze                DONE
funkcja print wywołana na obiekcie listy zawierającym 2 elementy [0, 1] wyświetli na ekranie "0 -> 1"           DONE
funkcja len wywołana na obiekcie listy zwróci jej długość                                                       DONE
'''


class Node:
    def __init__(self, value: Any):
        self.value: Any = value
        self.next: Node = Node


class LinkedList:
    def __init__(self):
        self.head: Node = Node
        self.tail: Node = Node

    def push(self, value: Any) -> None:
        # umieści nowy węzeł na początku listy
        pushed_node = Node(value)
        if self.head is None:
            self.head = pushed_node
            self.tail = self.head
        else:
            zapis = self.head
            self.head = pushed_node
            pushed_node.next = zapis

    def append(self, value: Any) -> None:
        # umieści nowy węzeł na końcu listy
        appended_node = Node(value)
        if self.head is None:
            self.head = appended_node
            self.tail = self.head
        else:
            iterator = self.head
            while iterator.next is not Node:
                iterator = iterator.next
            iterator.next = appended_node
            self.tail = iterator.next

    def node(self, at: int) -> Node:
        # zwróci węzeł znajdujący się na wskazanej pozycji
        if at <= 0:
            return self.head
        else:
            i = 0
            iterator = self.head
            while i is not at and iterator is not self.tail:
                iterator = iterator.next
                i += 1

            return iterator

    def insert(self, value: Any, after: Node) -> None:
        # wstawi nowy węzeł tuż za węzłem wskazanym w parametrze
        copy = self.head
        copy_end = self.head
        new_node = Node(value)

        while copy is not after:
            copy = copy.next
            copy_end = copy_end.next

        if copy_end.next is not Node:
            copy_end = copy_end.next
            new_node.next = copy_end
            copy.next = new_node
        else:
            self.append(value)

    def pop(self) -> Any:
        # usunie pierwszy element z listy i go zwróci
        popping_node = self.head
        after_pop = popping_node.next
        self.head = after_pop
        return popping_node.value

    def remove_last(self) -> Any:
        # usunie ostatni element z listy i go zwróci
        returning_value = self.tail.value
        iterator = self.head
        while iterator is not self.tail:
            iterator = iterator.next
        self.tail = iterator
        return returning_value

    def remove(self, after: Node) -> Any:
        # usunie z listy następnik węzła przekazanego w parametrze
        if after is self.tail:
            returning_value_at_end = self.remove_last()
            return returning_value_at_end

        iterator = self.head
        while iterator is not after:
            iterator = iterator.next
        returning_value = iterator.next.value
        iterator.next = iterator.next.next
        return returning_value

    def __str__(self):
        # wywołana na obiekcie listy zawierającym 2 elementy [0, 1] wyświetli na ekranie "0 -> 1"
        print_string = ""
        iterator = self.head
        while iterator is not self.tail:
            print_string += str(iterator.value)
            print_string += " -> "
            iterator = iterator.next
        print_string += str(iterator.value)
        return print_string

    def __len__(self):
        # wywołana na obiekcie listy zwróci jej długość
        if self.head is None:
            return 0
        iterator = self.head
        length = 0
        while iterator is not self.tail:
            length += 1
            iterator = iterator.next
        length += 1
        return length


list_ = LinkedList()
list_.head = None
assert list_.head is None

# method: push
list_.push(3)
print(list_.head.value)
list_.push(2)
print(list_.head.value, list_.head.next.value)
list_.push(1)
print(list_.head.value, list_.head.next.value, list_.head.next.next.value)
# print(f"\nlist_.tail.value = {list_.tail.value}")

# method: append
list_.append(4)
list_.append(5)
print(list_.head.value, list_.head.next.value, list_.head.next.next.value,
      list_.head.next.next.next.value, list_.head.next.next.next.next.value)
print(f"\nlist_.tail.value = {list_.tail.value}")
print(f"list_.head.value = {list_.head.value}")

# method: node
print("\nValue wezla 1: ", list_.node(1).value)
print("Value wezla 2: ", list_.node(2).value)
# jesli chcemy pobrac wezel, ktory wykracza poza liste - otrzymujemy ostatni wezel
print("Value wezla 7: ", list_.node(7).value)
# jesli chcemy pobrac wezel na ujemnym miejscu - otrzymamy pierwszy wezel
print("Value wezla -3: ", list_.node(-3).value)

# method: insert
wezel1 = list_.node(4)
list_.insert(999, wezel1)
print("\nValue wezla 0: ", list_.node(0).value)
print("Value wezla 1: ", list_.node(1).value)
print("Value wezla 2: ", list_.node(2).value)
print("Value wezla 3: ", list_.node(3).value)
print("Value wezla 4: ", list_.node(4).value)
print("Value wezla 5: ", list_.node(5).value)
print("Ogon: ", list_.tail.value)

# method: pop
first_element = list_.node(at=0)
returned_first_element = list_.pop()
print("\nValue wezla 0: ", list_.node(0).value)
print("Value wezla 1: ", list_.node(1).value)
print("Value wezla 2: ", list_.node(2).value)
print("Value wezla 3: ", list_.node(3).value)
print("Value wezla 4: ", list_.node(4).value)
print("Head: ", list_.head.value)

assert first_element.value == returned_first_element

# method: remove_last
last_element = list_.node(at=4)
returned_last_element = list_.remove_last()
assert last_element.value == returned_last_element
print("\nValue wezla 0: ", list_.node(0).value)
print("Value wezla 1: ", list_.node(1).value)
print("Value wezla 2: ", list_.node(2).value)
print("Value wezla 3: ", list_.node(3).value)
print("Value wezla 4: ", list_.node(4).value)

# created a new instance of a LinkedList
lista_pomocnicza = LinkedList()
lista_pomocnicza.head = None
lista_pomocnicza.append(0)
lista_pomocnicza.append(1)
lista_pomocnicza.append(2)
lista_pomocnicza.append(3)

# method: remove
after = lista_pomocnicza.node(1)
lista_pomocnicza.remove(after)
print("\nValue wezla l. pomoc. 0: ", lista_pomocnicza.node(0).value)
print("Value wezla l. pomoc. 1: ", lista_pomocnicza.node(1).value)
print("Value wezla l. pomoc. 2: ", lista_pomocnicza.node(2).value)

# method: __str__
print("\n__str__ of list_: ", list_)
print("__str__ of lista_pomocnicza: ", lista_pomocnicza)

# method: __len__
print("\n__len__ of list_: ", len(list_))
print("__len__ of lista_pomocnicza: ", len(lista_pomocnicza))
