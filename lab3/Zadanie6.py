# Zaimplementować funkcję prime(n: int) -> bool, która sprawdzi, czy liczba podana w parametrze jest liczbą pierwszą.
# Podpowiedź: wystarczy sprawdzić czy n jest podzielne przez wszystkie liczby poprzedzające


# def prime(n: int) -> bool:
#     if n > 0:
#         for i in range (2, n-1):
#             if n % i == 0:
#                 return False
#     return True

def prime(n: int, i=2) -> bool:
    if n == 2:
        return True
    else:
        if n % i == 0:
            return False
        else:
            if i+1 < n:
                if prime(n, i+1) is False:
                    return False
                else:
                    return True


# def prime(n: int) -> bool:
#     if n < 2:
#         return False
#     elif n == 2:
#         return True
#     else:
#         for i in range(2, n-1):
#             if n % i == 0:
#                 return False
#     return True

print(prime(10))
