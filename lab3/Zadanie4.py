# Zaimplementować funkcję reverse(txt: str) -> str, która zwróci odwrócony ciąg znaków przekazany w parametrze txt

# def reverse(txt: str) -> str:
#     if txt is None or txt == 0 or len(txt) == 0:
#         return txt
#     return reverse(txt[1:] + txt[0])

def reverse(txt) -> str:
    if len(txt) == 1:
        return txt
    else:
        return reverse(txt[1:]) + txt[0]


txt_reversed = reverse("123456789")
print(txt_reversed)
