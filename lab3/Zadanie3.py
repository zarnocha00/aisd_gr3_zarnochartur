# Zaimplementować funkcję power(number: int, n: int) -> int, której zadaniem jest zwrócenie wyniku działania number^n. NIE UŻYWAĆ OPERATORA **


def power(number: int, n: int) -> int:
    if n < 1:
        return 1
    else:

        return number * power(number, n-1)


print(f'5^3 = {power(5, 3)}')
