# Zaimplementować funkcję factorial(n: int) -> int, która zwróci silnię wartości przekazanej w parametrze


def factorial(n: int) -> int:
    if n < 1:
        return 1
    else:
        return n * factorial(n-1)


print(factorial(7))
