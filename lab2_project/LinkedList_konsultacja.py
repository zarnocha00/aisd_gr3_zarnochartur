from typing import Any

'''
klasa Node będzie reprezentacją węzła                                                                           DONE
klasa LinkedList będzie reprezentacją listy jednokierunkowej                                                    DONE
metoda push(self, value: Any) -> None umieści nowy węzeł na początku listy                                      DONE
metoda append(self, value: Any) -> None umieści nowy węzeł na końcu listy                                       DONE
metoda node(self, at: int) -> Node zwróci węzeł znajdujący się na wskazanej pozycji                             DONE
metoda insert(self, value: Any, after: Node) -> None wstawi nowy węzeł tuż za węzłem wskazanym w parametrze     DONE
metoda pop(self) -> Any usunie pierwszy element z listy i go zwróci                                             DONE
metoda remove_last(self) -> Any usunie ostatni element z listy i go zwróci                                      DONE
metoda remove(self, after: Node) -> Any usunie z listy następnik węzła przekazanego w parametrze                DONE
funkcja print wywołana na obiekcie listy zawierającym 2 elementy [0, 1] wyświetli na ekranie "0 -> 1"           DONE
funkcja len wywołana na obiekcie listy zwróci jej długość                                                       DONE
'''


class Node:
    def __init__(self, value: Any):
        self.value: Any = value
        self.next: Node = Node


class LinkedList:
    def __init__(self):
        self.head: Node = None
        self.tail: Node = None

    def push(self, value: Any) -> None:
        # umieści nowy węzeł na początku listy
        pushed_node = Node(value)
        if self.head is None:
            self.head = pushed_node
            self.tail = self.head
        else:
            zapis = self.head
            self.head = pushed_node
            pushed_node.next = zapis

    def append(self, value: Any) -> None:
        # umieści nowy węzeł na końcu listy
        appended_node = Node(value)
        if self.head is None:
            self.head = appended_node
            self.tail = self.head
        else:
            iterator = self.head
            while iterator.next is not Node:
                iterator = iterator.next
            iterator.next = appended_node
            self.tail = iterator.next

    def node(self, at: int) -> Node:
        # zwróci węzeł znajdujący się na wskazanej pozycji
        if at <= 0:
            return self.head
        else:
            i = 0
            iterator = self.head
            while i is not at and iterator is not self.tail:
                iterator = iterator.next
                i += 1

            return iterator

    def insert(self, value: Any, after: Node) -> None:
        # wstawi nowy węzeł tuż za węzłem wskazanym w parametrze
        copy = self.head
        copy_end = self.head
        new_node = Node(value)

        while copy is not after:
            copy = copy.next
            copy_end = copy_end.next

        if copy_end.next is not Node:
            copy_end = copy_end.next
            new_node.next = copy_end
            copy.next = new_node
        else:
            self.append(value)

    def pop(self) -> Any:
        # usunie pierwszy element z listy i go zwróci
        popping_node = self.head
        after_pop = popping_node.next
        self.head = after_pop
        return popping_node.value

    def remove_last(self) -> Any:
        # usunie ostatni element z listy i go zwróci
        returning_value = self.tail.value
        iterator = self.head
        while iterator.next is not self.tail:
            iterator = iterator.next
        # self.tail = iterator
        iterator.next = None
        self.tail = iterator
        return returning_value

    def remove(self, after: Node) -> Any:
        # usunie z listy następnik węzła przekazanego w parametrze
        if after is self.tail:
            returning_value_at_end = self.remove_last()
            return returning_value_at_end

        iterator = self.head
        while iterator is not after:
            iterator = iterator.next
        returning_value = iterator.next.value
        if iterator.next.next is not None:
            iterator.next = iterator.next.next
        else:
            iterator.next = None
            self.tail = iterator
        return returning_value

    def __str__(self):
        # wywołana na obiekcie listy zawierającym 2 elementy [0, 1] wyświetli na ekranie "0 -> 1"
        print_string = ""
        iterator = self.head
        while iterator is not self.tail:
            print_string += str(iterator.value)
            print_string += " -> "
            iterator = iterator.next
        print_string += str(iterator.value)
        return print_string

    def __len__(self):
        # wywołana na obiekcie listy zwróci jej długość
        if self.head is None:
            return 0
        iterator = self.head
        length = 0
        while iterator is not self.tail:
            length += 1
            iterator = iterator.next
        length += 1
        return length


# Proponowany schemat testów
list_ = LinkedList()

# Nowa lista jest pusta
assert list_.head is None

# Metoda push umieszcza elementy na początku listy
list_.push(1)
list_.push(0)
assert str(list_) == '0 -> 1'

# Metoda append umieszcza elementy na końcu listy
list_.append(9)
list_.append(10)
assert str(list_) == '0 -> 1 -> 9 -> 10'

# Element o indeksie 2 po wywołaniu metody insert będzie miał wartość 5
middle_node = list_.node(at=1)
list_.insert(5, after=middle_node)
assert str(list_) == '0 -> 1 -> 5 -> 9 -> 10'

# Metoda pop usuwa i zwraca pierwszy element listy
first_element = list_.node(at=0)
returned_first_element = list_.pop()
assert first_element.value == returned_first_element

# Metoda remove_last usuwa i zwraca ostatni element listy
last_element = list_.node(at=3)
returned_last_element = list_.remove_last()
assert last_element.value == returned_last_element
assert str(list_) == '1 -> 5 -> 9'

# Metoda remove przyjmuje węzeł jako argument, a następnie usuwa jego następnik
second_node = list_.node(at=1)
list_.remove(second_node)
assert str(list_) == '1 -> 5'
