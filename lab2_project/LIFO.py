from typing import Any


class Node:
    def __init__(self, value: Any):
        self.value: Any = value
        self.next: Node = Node


class LinkedList:
    def __init__(self):
        self.head: Node = None
        self.tail: Node = None

    def push(self, value: Any) -> None:
        # umieści nowy węzeł na początku listy
        pushed_node = Node(value)
        if self.head is None:
            self.head = pushed_node
            self.tail = self.head
        else:
            zapis = self.head
            self.head = pushed_node
            pushed_node.next = zapis

    def append(self, value: Any) -> None:
        # umieści nowy węzeł na końcu listy
        appended_node = Node(value)
        if self.head is None:
            self.head = appended_node
            self.tail = self.head
        else:
            iterator = self.head
            while iterator.next is not Node:
                iterator = iterator.next
            iterator.next = appended_node
            self.tail = iterator.next

    def node(self, at: int) -> Node:
        # zwróci węzeł znajdujący się na wskazanej pozycji
        if at <= 0:
            return self.head
        else:
            i = 0
            iterator = self.head
            while i is not at and iterator is not self.tail:
                iterator = iterator.next
                i += 1

            return iterator

    def insert(self, value: Any, after: Node) -> None:
        # wstawi nowy węzeł tuż za węzłem wskazanym w parametrze
        copy = self.head
        copy_end = self.head
        new_node = Node(value)

        while copy is not after:
            copy = copy.next
            copy_end = copy_end.next

        if copy_end.next is not Node:
            copy_end = copy_end.next
            new_node.next = copy_end
            copy.next = new_node
        else:
            self.append(value)

    def pop(self) -> Any:
        # usunie pierwszy element z listy i go zwróci
        popping_node = self.head
        after_pop = popping_node.next
        self.head = after_pop
        return popping_node.value

    def remove_last(self) -> Any:
        # usunie ostatni element z listy i go zwróci
        returning_value = self.tail.value
        iterator = self.head
        while iterator.next is not self.tail:
            iterator = iterator.next
        # self.tail = iterator
        iterator.next = None
        self.tail = iterator
        return returning_value

    def remove(self, after: Node) -> Any:
        # usunie z listy następnik węzła przekazanego w parametrze
        if after is self.tail:
            returning_value_at_end = self.remove_last()
            return returning_value_at_end

        iterator = self.head
        while iterator is not after:
            iterator = iterator.next
        returning_value = iterator.next.value
        if iterator.next.next is not None:
            iterator.next = iterator.next.next
        else:
            iterator.next = None
            self.tail = iterator
        return returning_value

    def __str__(self):
        # wywołana na obiekcie listy zawierającym 2 elementy [0, 1] wyświetli na ekranie "0 -> 1"
        print_string = ""
        iterator = self.head
        while iterator is not self.tail:
            print_string += str(iterator.value)
            print_string += " -> "
            iterator = iterator.next
        print_string += str(iterator.value)
        return print_string

    def __len__(self):
        # wywołana na obiekcie listy zwróci jej długość
        if self.head is None:
            return 0
        iterator = self.head
        length = 0
        while iterator is not self.tail:
            length += 1
            iterator = iterator.next
        length += 1
        return length


'''
• Klasa Stack będzie reprezentacją stosu
• do przechowywania wartości wewnątrz stosu wykorzystać własną implementację klasy LinkedList
• wywołanie inicjalizatora klasy Stack utworzy pusty stos
• metoda push(self, element: Any) -> None umieści nową wartość "na szczycie" stosu, 
czyli zostanie dodana na końcu wewnętrznej listy
• metoda pop(self) -> Any zwróci i usunie wartość ze szczytu stosu
• wywołanie funkcji print na obiekcie stosu wyświetli na ekranie jego elementy w postaci kolumny
• wywołanie funkcji len na obiekcie stosu zwróci liczbę elementów, które się w nim znajdują
'''


class Stack:
    def __init__(self):
        self._storage = LinkedList()

    def __len__(self):
        # zwróci liczbę elementów, które się w nim znajdują
        return len(self._storage)

    def __str__(self):
        # wyświetli na ekranie jego elementy w postaci kolumny
        print_string = ""
        iterator = self._storage.head
        while iterator is not self._storage.tail:
            print_string += str(iterator.value)
            print_string += "\n"
            iterator = iterator.next
        print_string += str(iterator.value)
        return print_string

    def push(self, element: Any) -> None:
        # umieści nową wartość "na szczycie" stosu, czyli zostanie dodana na końcu wewnętrznej listy
        self._storage.push(element)

    def pop(self) -> Any:
        # zwróci i usunie wartość ze szczytu stosu
        return self._storage.pop()


# Proponowany schemat testów:
stack = Stack()

# Nowo utworzony stos jest pusty
assert len(stack) == 0

# Dodanie kolejno 3 elementów tworzy stos
stack.push(3)
stack.push(10)
stack.push(1)
assert len(stack) == 3
print(f'Po pushach: \n{stack}')

# Szczytowa wartość na stosie ma wartość 1
top_value = stack.pop()
assert top_value == 1

# Stos po zdjęciu szczytowego elementu ma wartość 2
assert len(stack) == 2

print(f'\nPo popie: \n{stack}')
