from typing import Any, List, Callable, Union
from FIFO import Queue
import treelib


class TreeNode:
    def __init__(self, value):
        self.value: Any = value
        self.children: List['TreeNode'] = []

    def is_leaf(self) -> bool:
        if len(self.children) > 0:
            return False
        else:
            return True

    def add(self, child) -> None:
        self.children.append(child)

    def print_tree(self) -> None:
        print(self.value)
        if len(self.children) > 0:
            for child in self.children:
                child.print_tree()

    def search(self, value: Any) -> Union['TreeNode', None]:
        if self.value == value:
            return True
        else:
            if len(self.children) > 0:
                for child in self.children:
                    if child.search(value) is True:
                        return True
        return False

    def for_each_deep_first(self, visit: Callable[['TreeNode'], None]) -> None:
        visit(self)
        if len(self.children) > 0:
            for child in self.children:
                child.for_each_deep_first(visit)

    def show_for_each_deep_first(self) -> None:
        print(self.value)
        if len(self.children) > 0:
            for child in self.children:
                child.show_for_each_deep_first()

    def for_each_level_order(self, visit: Callable[['TreeNode'], None]) -> None:
        visit(self)
        kolej = Queue()
        if len(self.children) > 0:
            for child in self.children:
                kolej.enqueue(child)
            while len(kolej) > 1:
                visit(kolej.peek())
                child1 = kolej.dequeue()
                for child2 in child1.children:
                    kolej.enqueue(child2)

    def show_for_each_level_order(self) -> None:
        print(self.value)
        kolej = Queue()
        if len(self.children) > 0:
            for child in self.children:
                kolej.enqueue(child)
            while len(kolej) > 1:
                print(kolej.peek())
                child1 = kolej.dequeue()
                for child2 in child1.children:
                    kolej.enqueue(child2)

    def __str__(self):
        return self.value


class Tree:
    def __init__(self):
        self.root: TreeNode = TreeNode

    def for_each_level_order(self, visit: Callable[['TreeNode'], None]) -> None:
        self.root.for_each_level_order(visit)

    def for_each_deep_first(self, visit: Callable[['TreeNode'], None]) -> None:
        self.root.for_each_deep_first(visit)

    def print_tree(self):
        self.root.print_tree()

    def show(self):
        graf = treelib.Tree()
        graf.create_node(str(self.root.value), str(self.root.value))

        def add_node(node: 'TreeNode') -> None:
            for child in node.children:
                graf.create_node(str(child.value), str(child.value), parent=str(node.value))

        # self.for_each_level_order(add_node)
        self.for_each_deep_first(add_node)

        # graf.show(line_type="ascii")
        graf.show(line_type="ascii-exr")
        # graf.save2file("Tree.txt")


tree_ = Tree()
tree_.root = TreeNode("F")
tb = TreeNode("B")
tg = TreeNode("G")
tb.add(TreeNode("A"))

td = TreeNode("D")
td.add(TreeNode("C"))
td.add(TreeNode("E"))
tb.add(td)

ti = TreeNode("I")
ti.add(TreeNode("H"))
tg.add(ti)

tree_.root.add(tb)
tree_.root.add(tg)

print("tree_.root.show_for_each_deep_first():")
tree_.root.show_for_each_deep_first()

print("\n")

print("tree_.root.show_for_each_level_order():")
tree_.root.show_for_each_level_order()

print("\n")
print("tree_.print_tree(): ")
tree_.print_tree()

print(f'\ntree_.root.search("Z"): {tree_.root.search("Z")}')
print(f'tree_.root.search("E"): {tree_.root.search("E")}')
print(f'tree_.root.is_leaf(): {tree_.root.is_leaf()}')
print(f'tree_.root.children[0].children[0].is_leaf(): {tree_.root.children[0].children[0].is_leaf()}')

print("\ntree_.show()")
tree_.show()

# tree_.root = TreeNode("F")
# print(tree_.root.value)
#
# # "F" CHILDREN
# print("\n\"F\" CHILDREN")
# tree_.root.children.append(TreeNode("B"))
# tree_.root.children.append(TreeNode("G"))
# print(tree_.root.children[0].value, tree_.root.children[1].value)
#
# # "B" CHILDREN
# print("\n\"B\" CHILDREN")
# tree_.root.children[0].children.append(TreeNode("A"))
# tree_.root.children[0].children.append(TreeNode("D"))
# print(tree_.root.children[0].children[0].value, tree_.root.children[0].children[1].value)
#
# # "D" CHILDREN
# print("\n\"D\" CHILDREN")
# tree_.root.children[0].children[1].children.append(TreeNode("C"))
# tree_.root.children[0].children[1].children.append(TreeNode("E"))
# print(tree_.root.children[0].children[1].children[0].value, tree_.root.children[0].children[1].children[1].value)
#
# # "G" CHILDREN
# print("\n\"G\" CHILDREN")
# tree_.root.children[1].children.append(TreeNode("I"))
# print(tree_.root.children[1].children[0].value)
#
# # "I" CHILDREN
# print("\n\"I\" CHILDREN")
# tree_.root.children[1].children[0].children.append(TreeNode("H"))
# print(tree_.root.children[1].children[0].children[0].value)
#
# print(tree_.root.children[1].children[0].children[0].is_leaf())
