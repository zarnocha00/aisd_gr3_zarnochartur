from bubble_sort import *
from selection_sort import *
from insertion_sort import *

lista = [6, 3, 9, 0, 4]

print(f'Unsorted list: {lista}\n')

print(f'Bubble sort: {bubble_sort(lista)}')
print(f'Bubble sort reversed: {bubble_sort_reversed(lista)}\n')

print(f'Selection sort: {selection_sort(lista)}')
print(f'Selection sort reversed: {selection_sort_reversed(lista)}\n')

print(f'Insertion sort: {insertion_sort(lista)}')
print(f'Insertion sort reversed: {insertion_sort_reversed(lista)}')
