from typing import List


def bubble_sort(lista: List):
    for i in range(0, len(lista)):
        for j in range(0, len(lista)):
            if lista[i] < lista[j]:
                tmp = lista[i]
                lista[i] = lista[j]
                lista[j] = tmp
    return lista


def bubble_sort_reversed(lista: List):
    for i in range(0, len(lista)):
        for j in range(0, len(lista)):
            if lista[i] > lista[j]:
                tmp = lista[i]
                lista[i] = lista[j]
                lista[j] = tmp
    return lista
