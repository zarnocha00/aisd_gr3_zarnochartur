from typing import List


def insertion_sort(lista: List):
    n = len(lista)
    for i in range(1, n):
        key = lista[i]
        j = i-1
        while j >= 0 and lista[j] > key:
            lista[j+1] = lista[j]
            j = j-1
            lista[j+1] = key

    return lista


def insertion_sort_reversed(lista: List):
    n = len(lista)
    for i in range(1, n):
        key = lista[i]
        j = i-1
        while j >= 0 and lista[j] < key:
            lista[j+1] = lista[j]
            j = j-1
            lista[j+1] = key

    return lista
