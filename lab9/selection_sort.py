from typing import List


def selection_sort(lista: List):
    for i in range(0, len(lista)):
        min_index = i
        for j in range(i+1, len(lista)):
            if lista[j] < lista[min_index]:
                min_index = j
        tmp = lista[min_index]
        lista[min_index] = lista[i]
        lista[i] = tmp

    return lista


def selection_sort_reversed(lista: List):
    for i in range(0, len(lista)):
        max_index = i
        for j in range(i+1, len(lista)):
            if lista[j] > lista[max_index]:
                max_index = j
        tmp = lista[max_index]
        lista[max_index] = lista[i]
        lista[i] = tmp

    return lista
