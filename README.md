---

# LinkedList:


![LinkedList](https://bitbucket.org/zarnocha00/aisd_gr3_zarnochartur/raw/c286c33d48ad48a07e35f8f28c13a7091ab0415c/linked_list.png)

---

---

# Stack:


![Stack](https://bitbucket.org/zarnocha00/aisd_gr3_zarnochartur/raw/c286c33d48ad48a07e35f8f28c13a7091ab0415c/stack.png)

---

---

# Queue:


![Queue](https://bitbucket.org/zarnocha00/aisd_gr3_zarnochartur/raw/c286c33d48ad48a07e35f8f28c13a7091ab0415c/queue.png)

---